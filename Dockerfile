# Blender in Docker
#
# Author Soeren Metje

FROM ubuntu:20.04

WORKDIR /app

# Default values. Overwrite using --build-arg
ARG BLENDER_URL=https://download.blender.org/release/Blender2.91/blender-2.91.2-linux64.tar.xz

RUN apt update && apt install -y \
    libx11-dev \
    libxi-dev \
    libxxf86vm-dev \
    libxrender-dev \
    libgl1-mesa-glx \
    libglu1-mesa \
    curl \
    bzip2 \
    xz-utils

# donload, extract and create symlink to enable command blender ---------

# curl: silent,show error, redirect, to file | tar extract, automatic copression algo, file, to dir | find: use PWD to get absolute path
RUN curl -sSLO ${BLENDER_URL} && \
tar -xaf blender*.tar* -C /opt && \
rm -f blender*.tar* && \
ln -s `find "/opt" -type f -name blender` /usr/bin/blender

# LEGACY CMD:
# RUN curl -sSL ${BLENDER_URL} | tar -xJ -C /opt && ln -s /opt/blender-${BLENDER_VERSION}-linux64/blender /usr/bin/blender


ENTRYPOINT [ "blender", "-b" ]
CMD ["--help"]
